import fs from "fs";
import path from "path";
import Sequelize from "sequelize";

let db = null;

// this script auto load the models in ./models

module.exports = app => {
    if (!db) {
        const config = app.libs.config;
        const sequelize = new Sequelize(
            config.database,
            config.username,
            config.password,
            config.params
        );
        db = {
            sequelize,
            Sequelize,
            models: {}
        };
        // Parse files in models directory
        const dir = path.join(__dirname, "models");
        fs.readdirSync(dir).forEach(file => {
            const modelDir = path.join(dir, file);
            const model = sequelize.import(modelDir);
            //Append to db.models object filename as model name
            db.models[model.name] = model;
        });
        // Parse all models in model directory
        Object.keys(db.models).forEach(key => {
            Object.keys(db.models).forEach(key => {
                db.models[key].options.classMethods.associate(db.models);
            });
        });
    }
    return db;
};