module.exports = {
    database: "ntask",
    username: "",
    password: "",
    params: {
        operatorsAliases: false,
        dialect: "sqlite",
        storage: "ntask.sqlite",
        logging: false, // Avoid sql logging
        define: {
            underscored: true
        }
    },
    jwtSecret: "Nta$K-AP1",  // is a secret key that serves as a base to encode and decode tokens
    jwtSession: { session: false } // false to inform Passport that the API won't manage the session
};