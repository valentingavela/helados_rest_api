import logger from "./logger.js";

module.exports = {
    database: "ntask",
    username: "",
    password: "",
    params: {
        operatorsAliases: false,
        dialect: "sqlite",
        storage: "ntask.sqlite",
        logging: (sql) => {
            logger.info(`[${new Date()}] ${sql}`);
        },
        define: {
            underscored: true
        }
    },
    jwtSecret: "B0G20@@#Df",  // is a secret key that serves as a base to encode and decode tokens
    jwtSession: {session: false} // false to inform Passport that the API won't manage the session
};