import bodyParser from "body-parser";
import express from "express";
import morgan from "morgan";
import cors from "cors";
import helmet from "helmet";
import logger from "./logger.js";
import compression from "compression";

module.exports = app => {
    app.set('port', 3000);
    app.set('json spaces', 4);
    app.use(helmet());
    app.use(morgan("common", {
        stream: {
            write: (message) => {
                logger.info(message);
            }
        }
    }));
    app.use(bodyParser.json());
    app.use(app.auth.initialize());
    app.use(cors({
        origin: ["http://localhost:3001"],
        methods: ["GET", "POST", "PUT", "DELETE"],
        allowedHeaders: ["Content-Type", "Authorization"]
    }));
    app.use((req, res, next) => {
        //Middleware for preexecution of routes
        delete req.body.id; // Clean body.id for make db requrests
        next(); // Next is to warn express to execute the next function or the next middleware
    });
    app.use(express.static("public"));
    app.use(compression());
};