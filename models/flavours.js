module.exports = (sequelize, DataType) => {
    const Flavours = sequelize.define("Flavours", {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        ftype: {
            type: DataType.INTEGER,
            allowNull: false,
        }
    }, {
        classMethods: {
            associate: (models) => {
                Flavours.belongsTo(models.Users);
            }
        }
    });
    return Flavours;
}