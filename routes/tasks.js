module.exports = app => {
    const Tasks = app.db.models.Tasks;

    app.route('/tasks')
        // Use this function to allow or deny access to certain route.
        // When a client sends a valid token, their access will be successfully authenticated
        // and consequently, the object.req.user appears to be used inside the routes.
        // This object is only created when auth.js logic returns an authenticated user.
        .all(app.auth.authenticate()) 
        .get((req, res) => {
            Tasks.findAll({})
            .then(result => res.json(result))
            .catch(error => {
                res.status(412).json({msg: error.message});
            });
        })
        .post((req, res) => {
            Tasks.create(req.body) //Create new task
                .then(result => res.json(result)) // Response success
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        });

    app.route('/tasks/:id')
        .all(app.auth.authenticate()) // Use this function to allow or deny access to certain route
        .get((req, res) => {
            Tasks.findOne({where: req.params})
            .then(result => {
                if(result) {
                    res.json(result)
                } else {
                    res.sendStatus(404);
                }
            })
            .catch(error => {
                res.status(412).json({ msg: error.message });
            });
        })
        .put((req, res) => {
            Tasks.update(req.body, {where: req.params})
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        })
        .delete((req, res) => {
            Tasks.destroy({where: req.params})
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({msg: error.message});
                });
        });
};