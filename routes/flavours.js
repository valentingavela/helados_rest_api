module.exports = app => {
    const Flavours = app.db.models.Flavours;

    app.route('/flavours')
        .get((req, res) => {
            Flavours.findAll({})
            .then(result => res.json(result))
            .catch(error => {
                res.status(412).json({ msg: error.message });
            });
        })
        .post((req, res) => {
            Flavours.create(req.body)
                .then(result => res.json(result))
                .catch(error => {
                    res.status(412).json({ msg: error.message });
                });
        });

    app.route('/flavours/:id')
        .get((req, res) => {
            Flavours.findOne({ where: req.params })
            .then(result => {
                if(result) {
                    res.json(result)
                } else {
                    res.sendStatus(404);
                }
            })
            .catch(error => {
                res.status(412).json({ msg: error.message });
            });
        })
        .put((req, res) => {
            Flavours.update(req.body, { where: req.params })
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({ msg: error.message });
                });
        })
        .delete((req, res) => {
            Flavours.destroy({ where: req.params })
                .then(result => res.sendStatus(204))
                .catch(error => {
                    res.status(412).json({ msg: error.message });
                });
        });
};