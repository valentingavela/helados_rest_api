/**
 * @api {post} /token Authentication Token
 * @apiGroup Credentials
 * @apiParam {String} email User email
 * @apiParam {String} password User password
 * @apiParamExample {json} Input
 * {
 *  "email": "john@connor.net",
 *  "password": "123456"
 * }
 * @apiSuccess {String} token Token of authenticated user
 * @apiSuccessExample {json} Success
 * HTTP/1.1 200 OK
 * {"token": "xyz.abc.123.hgf"}
 * @apiErrorExample {json} Authentication error
 * HTTP/1.1 401 Unauthorized
 */

import jwt from "jwt-simple";
import bcrypt from "bcrypt";

// this route is responsible for generating an encoded token with a ***payload***,
// given to the user that sends right email and pwd via req.body.email and req.body.password
// the payload is going to have only the user id. 
// Token generation is by the jwt.encode(payload, cfg.jwtSecret)  function
// that use the same secret key jwtSecret that was created on the libs/config.js
module.exports = app => {
    const cfg = app.libs.config;
    const Users = app.db.models.Users;
    app.post('/token', (req, res) => {
        if(req.body.email && req.body.password) {
            const email = req.body.email;
            const password = req.body.password;
            Users.findOne({where: {email: email}})
            .then(user => {
                if (bcrypt.compareSync(password, user.password)){
                // if (Users.isPassword(user.password, password)) {
                    const payload = {id: user.id};
                    res.json({
                        token: jwt.encode(payload, cfg.jwtSecret)
                    });
                } else {
                    // console.log("FAIL!");
                    res.sendStatus(401);   
                }
            })
            .catch(
                error => res.sendStatus(401)
                );
        } else {
            // console.log("ERROR 2");
            res.sendStatus(401);
        }
    });
};