describe('Routes: Index', () => {
    describe('GET /', () => {
        it('returns the API status', done => {
            request.get('/')    // Make get request
            .expect(200)        // if server status is 200
            .end((err, res) => {
                const expeted = { status: 'Ntask API' }; // Expected response.
                expect(res.body).to.eql(expeted);
                done(err);
            });
        })
    })
});