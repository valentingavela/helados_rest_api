// We need to test four conditions
// • Request authenticated by a valid user.
// • Request with a valid e - mail but with the wrong password.
// • Request with an unregistered e - mail.
// • Request without an e - mail and password.
describe("Routes: Token", () => {
    const Users = app.db.models.Users;
    describe("POST /token", () => {
        beforeEach(done => {
            // Runs before each test...
            Users
                .destroy({where: {}}) // Clear table users
                .then(() => Users.create({ // Create one valid users
                    name: 'john',
                    email: 'john@mail.com',
                    password: '12345'
                }))
                .then(() => done());
        });
        describe("expect status 200", () => {
            it("returns authenticated user token", done => {
                request.post('/token')
                    .send({
                        email: 'john@mail.com',
                        password: '12345'
                    })
                    .expect(200)
                    .end((err, res) => {
                        expect(res.body).to.include.keys("token");
                        done(err);
                    });
            });
        });
        describe("status 401", () => {
            it("throws error when password is incorrect", done => {
                request.post('/token')
                    .send({
                        email: 'john@mail.com',
                        password: 'wrong pass'
                    })
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });
            });
            it("throws error when email not exist", done => {
                request.post('/token')
                    .send({
                        email: 'johsn@mail.com',
                        password: '12345'
                    })
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });  
            });
            it("throws error when email and password are blank", done => {
                request.post('/token')
                    .expect(401)
                    .end((err, res) => {
                        done(err);
                    });  
            });
        });
    });
});