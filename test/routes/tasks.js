// Detailing how to cheat the authentication part, we are going to reuse the module
// jwt - simple to create a valid token that will be used in the header of all the tests.This
// token will be repeatedly generated within the callback of the function beforeEach(done).
// To generate it, though, we have to delete all users first, using the function Users.
// destroy({ where: {} }) and then create a new one via the Users.create() function.
// We’ll do the same with task creation, but instead of using the function Tasks.create()
// it will be the function Tasks.bulkCreate(), which allows sending an array of tasks to be
// inserted in a single execution(this function is very useful for inclusion in a plot of data).
// The tasks are going to use the user.id field, created to ensure that they are from
// the authenticated user.In the end, let’s use the first task created by the piece fakeTask
//     = tasks[0] to reuse its id on the tests that need a task id as a route parameter.We’ll
// generate a valid token using the function jwt.encode({ id: user.id }, jwtSecret).
// Both the objects fakeTask and token are created in a scope above the function
//     beforeEach(done), so they can be reused on the tests.To understand in detail, you need
// to write the following implementation.


import jwt from "jwt-simple";

describe('Routes: Tasks', () => {
    const Users = app.db.models.Users;
    const Tasks = app.db.models.Tasks;
    const jwtSecret = app.libs.config.jwtSecret;
    let token;
    let fakeTask;
    beforeEach(done => {
        Users
            .destroy({where: {}})
            .then(() => Users.create({
                name :'John',
                email: 'john@mail.net',
                password: '12345'
            }))
    });
    describe('GET /tasks', () => {
        describe('status 200', () => {
            it('returns a list of tasks', done => {
               
            });
        });
    });
    describe('POST /tasks/', () => {
        describe('status 200', () => {
            it('creates a new task', done => {
                
            });
        });
    });
    describe('Get /tasks/:id', () => {
        describe('status 200', () => {
            it('Returns one task', done => {

            });
        });
        describe('status 404', () => {
            it('throws error when task not exist', done => {

            });
        });
    });
    describe('PUT /tasks/:id', () => {
        describe('status 204', () => {
            it('updates a task', done => {

            });
        });
    });
    describe('DELETE /tasks/:id', () => {
        describe('Status 204', () => {
            it('Removes a task', done => {

            });
        });
    });

});